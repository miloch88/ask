package pl.sda.askplus.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.askplus.model.AppUser;
import pl.sda.askplus.model.Quiz;
import pl.sda.askplus.model.dto.AddQuizDto;
import pl.sda.askplus.model.dto.ModifyQuizDto;
import pl.sda.askplus.repository.RegisterRepository;
import pl.sda.askplus.repository.QuizRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class QuizService {

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private RegisterRepository registerRepository;

    @Autowired
    private LoginService loginService;

    public Optional<Quiz> addQuiz(AddQuizDto dto) {
        return addQuiz(dto.getName(), dto.getValidationTime());
    }

    private Optional<Quiz> addQuiz(String name, String validationTime) {

        if(name != null && validationTime != null){

            Quiz quiz = new Quiz();
            AppUser appUser = loginService.getLoggedInUser().get();

            quiz.setName(name);

            LocalDateTime now = LocalDateTime.now();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
            String formatDateTime = now.format(formatter);
            quiz.setCreateDate(LocalDateTime.parse(formatDateTime, formatter));

            quiz.setValidationTime(LocalDateTime.parse(validationTime, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")));

            quiz.setAddress(UUID.randomUUID().toString());

            quiz.setOwner(appUser);
            quizRepository.save(quiz);

            appUser.getQuizList().add(quiz);
            registerRepository.save(appUser);

            return Optional.of(quiz);
        }
        return Optional.empty();
    }

    public List<Quiz> getAll() {
        return quizRepository.findAll();
    }

    public Optional<Quiz> getById(Long quizId) {
        return quizRepository.findById(quizId);
    }

    public Optional<Quiz> modifyQuiz(ModifyQuizDto dto) {
        Optional<Quiz> quizOptional = quizRepository.findById(dto.getQuizId());

        if(quizOptional.isPresent()){

            Quiz quiz = quizOptional.get();

            if(!dto.getName().isEmpty()){
                quiz.setName(dto.getName());
            }

            if(!dto.getValidationTime().isEmpty()){
                quiz.setValidationTime(LocalDateTime.parse(dto.getValidationTime().toString(),
                        DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")));
            }

            quiz = quizRepository.save(quiz);

            return Optional.of(quiz);
        }
        return Optional.empty();
    }

    public Optional<Quiz> getByUniqeNumber(String address) {
        return quizRepository.findByAddress(address);
    }
}

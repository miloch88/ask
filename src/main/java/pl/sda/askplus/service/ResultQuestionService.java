package pl.sda.askplus.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.askplus.model.*;
import pl.sda.askplus.model.dto.quiz.AnswerDto;
import pl.sda.askplus.model.dto.quiz.QuizQuestionDto;
import pl.sda.askplus.repository.QuestionRepository;
import pl.sda.askplus.repository.ResultQuestionRepository;
import pl.sda.askplus.repository.ResultQuizRepository;
import pl.sda.askplus.repository.SelectionsRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ResultQuestionService {

    @Autowired
    private QuizService quizService;

    @Autowired
    private ResultQuizService resultQuizService;

    @Autowired
    private SelectionsService selectionsService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private ResultQuestionRepository resultQuestionRepository;

    @Autowired
    private ResultQuizRepository resultQuizRepository;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private SelectionsRepository selectionsRepository;

    public Optional<ResultQuestion> addResultQuestion(QuizQuestionDto dto,
                                                      ResultQuiz resultQuiz,
                                                      Long quizid,
                                                      Long questionNumber) {

        ResultQuestion newResultQuestion = new ResultQuestion();

        if (dto.getAnswerId() != null) {

            Question question = quizService.getById(quizid).get().getQuestionList().get(Math.toIntExact(questionNumber));

            Selections selection = selectionsService.findById(dto.getAnswerId()).get();

            newResultQuestion.setResultQuiz(resultQuiz);
            newResultQuestion.setQuestion(question);
            newResultQuestion.setYourAnswer(selection);
            newResultQuestion = resultQuestionRepository.save(newResultQuestion);


            question.getResultQuestionList().add(newResultQuestion);

            questionRepository.save(question);

            resultQuiz.getResultQuestionList().add(newResultQuestion);
            resultQuizRepository.save(resultQuiz);

            selection.getResultQuestionList().add(newResultQuestion);
            selectionsRepository.save(selection);

            return Optional.of(newResultQuestion);
        }

        if (!dto.getAnswerDtoList().isEmpty()) {

            Question question = quizService.getById(quizid).get().getQuestionList().get(Math.toIntExact(questionNumber));

            for (AnswerDto a : dto.getAnswerDtoList()) {

                if (a.isChecked()) {

                    Selections selection = selectionsService.findById(a.getAnswerId()).get();

                    newResultQuestion.setResultQuiz(resultQuiz);
                    newResultQuestion.setQuestion(question);
                    newResultQuestion.getYourAnswersList().add(selection);
                    newResultQuestion = resultQuestionRepository.save(newResultQuestion);

                    question.getResultsQuestionList().add(newResultQuestion);
                    questionRepository.save(question);

                    resultQuiz.getResultQuestionList().add(newResultQuestion);
                    resultQuizRepository.save(resultQuiz);
//
////                selection.getResultQuestionList().add(newResultQuestion);
//                    selection.getResultsQuestionList().add(newResultQuestion);
//                    selectionsRepository.save(selection);

                }
            }

        }


        return Optional.of(newResultQuestion);
    }


    public List<ResultQuestion> getAll() {
        return resultQuestionRepository.findAll();
    }
}

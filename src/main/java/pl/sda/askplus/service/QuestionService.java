package pl.sda.askplus.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.askplus.model.Question;
import pl.sda.askplus.model.Quiz;
import pl.sda.askplus.model.dto.AddQuestionDto;
import pl.sda.askplus.model.dto.ModifyQuestionDto;
import pl.sda.askplus.model.dto.ModifyQuizDto;
import pl.sda.askplus.repository.QuestionRepository;
import pl.sda.askplus.repository.QuizRepository;

import java.util.Optional;

@Service
public class QuestionService {

    @Autowired
    private QuizService quizService;

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private QuestionRepository questionRepository;


    public Optional<Question> addQuestion(AddQuestionDto dto) {
        return addQuestion(dto.getQuizId(), dto.getQuestion(), dto.getType());
    }

    private Optional<Question> addQuestion(Long quizId, String question, String type) {

        if(quizId != null && !question.isEmpty()){

            Question questionNew = new Question();
            Quiz quiz = quizService.getById(quizId).get();

            questionNew.setQuestion(question);
            questionNew.setQuiz(quiz);
            questionNew.setType(type);
            questionRepository.save(questionNew);

            quiz.getQuestionList().add(questionNew);
            quizRepository.save(quiz);

            return Optional.of(questionNew);
        }
        return Optional.empty();
    }

    public Optional<Question> getById(Long questionId) {
        return questionRepository.findById(questionId);
    }


    public Optional<Question> modifyQuestion(ModifyQuestionDto dto) {
        Optional<Question> questionOptional = questionRepository.findById(dto.getQuestionId());

        if(questionOptional.isPresent()){

            Question question = questionOptional.get();

            if(!dto.getQuestionText().isEmpty()){
                question.setQuestion(dto.getQuestionText());
            }

            question = questionRepository.save(question);

            return Optional.of(question);
        }
        return Optional.empty();
    }
}

package pl.sda.askplus.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.askplus.model.Selections;
import pl.sda.askplus.model.Question;
import pl.sda.askplus.model.dto.AddSelectionsDto;
import pl.sda.askplus.model.dto.ModifySelectionDto;
import pl.sda.askplus.repository.SelectionsRepository;
import pl.sda.askplus.repository.QuestionRepository;

import java.util.Optional;

@Service
public class SelectionsService {

    @Autowired
    private QuestionService questionService;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private SelectionsRepository selectionsRepository;

    public Optional<Selections> addQuestion(AddSelectionsDto dto) {
        return addQuestion(dto.getQuestionId(), dto.getText(), dto.isCorrect());
    }

    private Optional<Selections> addQuestion(Long questionId, String text, boolean correct) {

        if(questionId != null && !text.isEmpty()){

            Selections selections = new Selections();
            Question question = questionService.getById(questionId).get();

            selections.setText(text);
            selections.setCorrect(correct);
            selections.setQuestion(question);
            selections = selectionsRepository.save(selections);

            question.getSelectionsList().add(selections);
            questionRepository.save(question);

            return Optional.of(selections);
        }
        return Optional.empty();
    }

    public Optional<Selections> findById(Long answerId) {
        return selectionsRepository.findById(answerId);
    }

    public Optional<Selections> modifySelection(ModifySelectionDto dto) {
        Optional<Selections> selectionsOptional = selectionsRepository.findById(dto.getSelectionId());

        if(selectionsOptional.isPresent()){

            Selections selection = selectionsOptional.get();

            if(!dto.getText().isEmpty()){
                selection.setText(dto.getText());
            }

            selection.setCorrect(dto.isCorrect());

            selection = selectionsRepository.save(selection);

            return Optional.of(selection);
        }
        return Optional.empty();
    }

}

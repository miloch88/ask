package pl.sda.askplus.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.askplus.model.*;
import pl.sda.askplus.model.dto.AddResultQuizDto;
import pl.sda.askplus.model.dto.statistic.StatisticQuizDto;
import pl.sda.askplus.repository.AppUserRepository;
import pl.sda.askplus.repository.QuizRepository;
import pl.sda.askplus.repository.ResultQuizRepository;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class ResultQuizService {

    @Autowired
    private LoginService loginService;

    @Autowired
    private ResultQuizRepository resultQuizRepository;

    @Autowired
    private QuizService quizService;

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private AppUserRepository appUserRepository;

    public Optional<ResultQuiz> findById(Long id) {
        return resultQuizRepository.findById(id);
    }

    public Optional<ResultQuiz> addResultQuiz(AddResultQuizDto dto) {
        return addResultQuiz(dto.getQuizId(), dto.getUniqeNumber());
    }

    private Optional<ResultQuiz> addResultQuiz(Long quizId, String uniqeNumber) {

        if (quizId != null && !uniqeNumber.isEmpty()) {

            ResultQuiz newResult = new ResultQuiz();
            newResult.setQuiz(quizService.getById(quizId).get());

            if (!loginService.getLoggedInUser().isPresent()) {
                AppUser unknow = appUserRepository.findById(Long.valueOf(2)).get();
                newResult.setAppUser(unknow);
            } else {
                newResult.setAppUser(loginService.getLoggedInUser().get());
            }

            newResult.setUniqeNumber(uniqeNumber);

            resultQuizRepository.save(newResult);

            Quiz quiz = quizService.getById(quizId).get();
            quiz.getResultQuizsList().add(newResult);
            quizRepository.save(quiz);


            AppUser appUser = new AppUser();

            if (!loginService.getLoggedInUser().isPresent()) {
                appUser = (appUserRepository.findById(Long.valueOf(2)).get());
            } else {
                appUser = (loginService.getLoggedInUser().get());
            }

            newResult.setAppUser(appUser);
            appUser.getResultQuizsList().add(newResult);
            appUserRepository.save(appUser);

            return Optional.of(newResult);
        }
        return Optional.empty();
    }

    public Optional<ResultQuiz> getByUniqueNumber(String uniqeNumber) {
        return resultQuizRepository.getByUniqeNumber(uniqeNumber);
    }

    public List<ResultQuiz> getAll() {
        return resultQuizRepository.findAll();
    }


    public Integer calculateScore(ResultQuiz resultQuiz) {

        int socre = 0;

        for (ResultQuestion resultQuestion : resultQuiz.getResultQuestionList()) {
            for (Selections selection : resultQuestion.getQuestion().getSelectionsList()) {
                if (selection.isCorrect() && resultQuestion.getYourAnswer().getId() == selection.getId()) {
                    socre += 1;
                }

            }
        }
        return socre;
    }

    public ResultQuiz setCompletedTime(ResultQuiz resultQuiz) {
        resultQuiz.setCompletedTime(LocalDateTime.now());
        resultQuizRepository.save(resultQuiz);

        return resultQuiz;
    }

    public Double calculateGeneralScore(Quiz quiz) {
        double generalScore = 0;
        int total = 0;

        for (ResultQuiz result : quiz.getResultQuizsList()) {
            generalScore += calculateScore(result);
            total += result.getResultQuestionList().size();
        }
        return 100 * (generalScore / total);
    }

    public Integer getCompleted(Quiz quiz) {
        int completed = 0;

        for (ResultQuiz rq : quiz.getResultQuizsList()) {
            if (rq.getQuiz().getQuestionList().size() == rq.getResultQuestionList().size()) {
                completed += 1;
            }
        }
        return completed;
    }

    public List<Integer> getScoreList(List<ResultQuiz> resultQuizsList) {
        List<Integer> list = new ArrayList<>();
        for (ResultQuiz r : resultQuizsList) {
            list.add((calculateScore(r)));
        }
        return list;
    }

    public List<StatisticQuizDto> createStatisticListDto(List<ResultQuiz> resultQuiz) {
        List<StatisticQuizDto> statisticQuizDtoList = new ArrayList<>();

        for (ResultQuiz rq : resultQuiz) {
            StatisticQuizDto statisticQuizDto = new StatisticQuizDto();

            statisticQuizDto.setResultQuiz(rq);

            Integer score = calculateScore(rq);
            statisticQuizDto.setScore(score);

            statisticQuizDtoList.add(statisticQuizDto);
        }
        return statisticQuizDtoList;
    }

    public List<StatisticQuizDto> comparableByUser(List<StatisticQuizDto> statisticQuizDtoList) {

        Collections.sort(statisticQuizDtoList, new Comparator<StatisticQuizDto>() {
            @Override
            public int compare(StatisticQuizDto o1, StatisticQuizDto o2) {
                return o1.getResultQuiz().getAppUser().getName().compareTo(o2.getResultQuiz().getAppUser().getName());
            }
        });
        return statisticQuizDtoList;
    }

    public List<StatisticQuizDto> comparableByDate(List<StatisticQuizDto> statisticQuizDtoList) {

        Collections.sort(statisticQuizDtoList, new Comparator<StatisticQuizDto>() {
            @Override
            public int compare(StatisticQuizDto o1, StatisticQuizDto o2) {

                return o1.getResultQuiz().getCompletedTime().compareTo(o2.getResultQuiz().getCompletedTime());
            }
        });
        return statisticQuizDtoList;
    }

    public List<StatisticQuizDto> comparableByScore(List<StatisticQuizDto> statisticQuizDtoList) {

        Collections.sort(statisticQuizDtoList, new Comparator<StatisticQuizDto>() {
            @Override
            public int compare(StatisticQuizDto o1, StatisticQuizDto o2) {

                if (o1.getScore() > o2.getScore()) {
                    return -1;
                } else if (o1.getScore() < o2.getScore()) {
                    return 1;
                }
                return 0;

            }
        });
        return statisticQuizDtoList;
    }

}

package pl.sda.askplus.service.random;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.askplus.model.Question;
import pl.sda.askplus.model.Quiz;
import pl.sda.askplus.model.Selections;
import pl.sda.askplus.model.dto.AddQuestionDto;
import pl.sda.askplus.model.dto.AddQuizDto;
import pl.sda.askplus.model.dto.AddSelectionsDto;
import pl.sda.askplus.model.dto.random.RandomDto;
import pl.sda.askplus.model.random.SingleQuestion;
import pl.sda.askplus.service.QuestionService;
import pl.sda.askplus.service.QuizService;
import pl.sda.askplus.service.SelectionsService;

import javax.persistence.criteria.CriteriaBuilder;
import java.io.*;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class RandomService {

    @Autowired
    private QuizService quizService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private SelectionsService selectionsService;

    public List<String> getCategoryList() {

        List<String> categoryList = new ArrayList<>();

        File folder = new File("src/main/resources/questions");
        File[] allFiles = folder.listFiles();
        for (File f : allFiles) {

            String categoryName = f.getName().replaceAll(".txt", "")
                    .replaceAll("_", " ");
            categoryList.add(categoryName);
        }
        return categoryList;
    }

    public Optional<Quiz> createNewQuiz(RandomDto dto) {

        Map<String, List<SingleQuestion>> categories = readAllCategoriesQuestions();

        List<SingleQuestion> selectedCategoryQuestions = categories.get(dto.getCategory());

//        Zabezpieczneie przed liczbą pytań
        if(dto.getNumber() > selectedCategoryQuestions.size()){
            dto.setNumber((long) selectedCategoryQuestions.size());
        }

        Collections.shuffle(selectedCategoryQuestions);

//        Nowy quiz
        AddQuizDto quizDto = new AddQuizDto(dto.getCategory(), dto.getValidationTime());
        Optional<Quiz> randomQuiz = quizService.addQuiz(quizDto);


        int numberTemp= Math.toIntExact(dto.getNumber());
        for (int i = 0; i < numberTemp; i++) {

            SingleQuestion quizQuestions = selectedCategoryQuestions.get(i);
            String randomQuestion = quizQuestions.getQuestion();

//        Nowe pytanie
            AddQuestionDto questionDto = new AddQuestionDto(randomQuiz.get().getId(), randomQuestion, "single");

            Question question = questionService.addQuestion(questionDto).get();

//        Nowe odpowiedzi
            String goodAnswer = (quizQuestions.getAnswers().get(0));
            Collections.shuffle(quizQuestions.getAnswers());

            for (int j = 0; j < quizQuestions.getAnswers().size(); j++) {
                String answer = quizQuestions.getAnswers().get(j);

                AddSelectionsDto selectionsDto = new AddSelectionsDto(answer, false, question.getId());
                if(answer.equals(goodAnswer)){
                    selectionsDto.setCorrect(true);
                }
                Selections selection = selectionsService.addQuestion(selectionsDto).get();
            }
        }
        return randomQuiz;
    }

    private Map<String, List<SingleQuestion>> readAllCategoriesQuestions() {
        Map<String, List<SingleQuestion>> categories = new HashMap<>();

        File folder = new File("src\\main\\resources\\questions");
        File[] allFiles = folder.listFiles();
        for (File f : allFiles) {
            List<SingleQuestion> categoryQuestions = readAllQuizQuestionFromFile(f);

            String categoryName = f.getName().replaceAll(".txt", "")
                    .replaceAll("_", " ");
            categories.put(categoryName, categoryQuestions);
        }
        return categories;
    }

    private List<SingleQuestion> readAllQuizQuestionFromFile(File file) {

        List<SingleQuestion> allQuestions = new ArrayList<>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                SingleQuestion sq = new SingleQuestion();
                sq.setQuestion(line);
                int answerNumber = Integer.parseInt(br.readLine());
                for (int i = 0; i < answerNumber; i++) {
                    sq.getAnswers().add(br.readLine());
                }
                allQuestions.add(sq);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allQuestions;
    }
}

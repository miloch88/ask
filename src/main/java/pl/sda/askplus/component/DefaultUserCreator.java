package pl.sda.askplus.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import pl.sda.askplus.model.AppUser;
import pl.sda.askplus.model.dto.RegisterDto;
import pl.sda.askplus.service.RegisterService;

import java.util.Optional;

@Lazy
@Component
public class DefaultUserCreator {

    @Autowired
    public DefaultUserCreator(RegisterService registerService) {
        Optional<AppUser> clientOptional = registerService.findByEmail("admin@email.com");
        if (!clientOptional.isPresent()) {

            RegisterDto adminDto = new RegisterDto();

            adminDto.setName("Admin");
            adminDto.setSurname("Admin");
            adminDto.setPassword("admin");
            adminDto.setPasswordConfirm("admin");
            adminDto.setEmail("admin@email.com");

            registerService.registerAppUser(adminDto);
        }

        Optional<AppUser> unknowOptional = registerService.findByEmail("unknow@email.com");
        if (!unknowOptional.isPresent()) {

            RegisterDto unknowDto = new RegisterDto();

            unknowDto.setName("Unknow");
            unknowDto.setSurname("Unknow");
            unknowDto.setPassword("unknow");
            unknowDto.setPasswordConfirm("unknow");
            unknowDto.setEmail("unknow@email.com");

            registerService.registerAppUser(unknowDto);
        }
    }

}

package pl.sda.askplus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.askplus.model.Quiz;

import java.util.Optional;

public interface QuizRepository extends JpaRepository<Quiz, Long> {
    Optional<Quiz> findByAddress(String address);
}

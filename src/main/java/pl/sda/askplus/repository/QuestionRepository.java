package pl.sda.askplus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.askplus.model.Question;

public interface QuestionRepository extends JpaRepository<Question, Long> {
}

package pl.sda.askplus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.askplus.model.AppUser;

import java.util.Optional;

public interface RegisterRepository extends JpaRepository<AppUser, Long> {

    Optional<AppUser> findByEmail(String email);
}

package pl.sda.askplus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.askplus.model.ResultQuestion;
import pl.sda.askplus.model.ResultQuiz;

public interface ResultQuestionRepository extends JpaRepository<ResultQuestion, Long> {
}

package pl.sda.askplus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.askplus.model.ResultQuiz;

import java.util.List;
import java.util.Optional;

public interface ResultQuizRepository extends JpaRepository<ResultQuiz, Long> {


    Optional<ResultQuiz> getByUniqeNumber(String uniqeNumber);
}

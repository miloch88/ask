package pl.sda.askplus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.askplus.model.Selections;

public interface SelectionsRepository extends JpaRepository<Selections, Long> {
}

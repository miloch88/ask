package pl.sda.askplus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.askplus.model.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {
}

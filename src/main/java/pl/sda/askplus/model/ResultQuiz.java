package pl.sda.askplus.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.org.glassfish.gmbal.ManagedAttribute;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class ResultQuiz {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonIgnore
    @ManyToOne
    private Quiz quiz;

    @JsonIgnore
    @ManyToOne
    private AppUser appUser;

//    @JsonIgnore
//    @ManyToOne
//    private Question question;

    //single
    @JsonIgnore
    @OneToMany
    private List<ResultQuestion> resultQuestionList = new ArrayList<>();

    //many
    @JsonIgnore
    @ManyToMany
    private List<ResultQuestion> resultQuestionFromResultQuiz;

    private String uniqeNumber;

    private LocalDateTime completedTime;

}

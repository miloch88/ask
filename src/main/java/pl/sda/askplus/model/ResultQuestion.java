package pl.sda.askplus.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ResultQuestion {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    //single
    @JsonIgnore
    @ManyToOne
    private ResultQuiz resultQuiz;

    //Many
    @JsonIgnore
    @ManyToMany(mappedBy = "resultQuestionFromResultQuiz")
    private List<ResultQuiz> resultQuizList;

//    Single
    @JsonIgnore
    @ManyToOne
    private Question question;

//    Many
    @JsonIgnore
    @ManyToMany
    private List<Question> resultQuestionList;

//    single
    @JsonIgnore
    @ManyToOne
    private Selections yourAnswer;

//    many
    @JsonIgnore
    @ManyToMany(mappedBy = "resultsQuestionList")
    private List<Selections> yourAnswersList = new ArrayList<>();

}

package pl.sda.askplus.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class Selections {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String text;

    private boolean correct;

    @JsonIgnore
    @ManyToOne
    private Question question;

    @JsonIgnore
    @OneToMany
    private List<ResultQuestion> resultQuestionList;

    @JsonIgnore
    @ManyToMany
    private List<ResultQuestion> resultsQuestionList;

}

package pl.sda.askplus.model.random;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SingleQuestion {

    private String question = null;
    private List<String> answers = new ArrayList<>();
}

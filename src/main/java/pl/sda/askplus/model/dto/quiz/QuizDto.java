package pl.sda.askplus.model.dto.quiz;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuizDto {

    private Long quizId;

    private List<QuizQuestionDto> quizQuestions = new ArrayList<>();

}

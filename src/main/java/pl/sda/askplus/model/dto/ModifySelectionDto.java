package pl.sda.askplus.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ModifySelectionDto {

    @NotNull
    private Long selectionId;

    private String text;

    private boolean correct;
}

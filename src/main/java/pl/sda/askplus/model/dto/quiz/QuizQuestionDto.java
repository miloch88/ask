package pl.sda.askplus.model.dto.quiz;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuizQuestionDto {

    private String text;

    private List<AnswerDto> answerDtoList = new ArrayList<>();

    private Long answerId;

    ////////////////////
    private String type;
    ////////////////////

}

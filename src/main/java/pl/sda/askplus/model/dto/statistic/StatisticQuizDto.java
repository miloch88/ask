package pl.sda.askplus.model.dto.statistic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.sda.askplus.model.ResultQuiz;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StatisticQuizDto {

    private ResultQuiz resultQuiz;

    private Integer score;

}

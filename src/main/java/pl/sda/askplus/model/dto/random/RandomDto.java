package pl.sda.askplus.model.dto.random;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RandomDto {

    private String category;

    private Long number;

    private String validationTime;
}

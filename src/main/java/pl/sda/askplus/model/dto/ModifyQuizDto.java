package pl.sda.askplus.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ModifyQuizDto {

    @NotNull
    private Long quizId;

    private String name;
    private String validationTime;

}

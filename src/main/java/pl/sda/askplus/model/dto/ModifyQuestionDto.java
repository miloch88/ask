package pl.sda.askplus.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ModifyQuestionDto {

    @NotNull
    private Long questionId;

    private String questionText;
}

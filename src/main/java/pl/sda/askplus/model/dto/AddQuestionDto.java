package pl.sda.askplus.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AddQuestionDto {

    @NotNull
    private Long quizId;

    @NotEmpty
    private String question;

    @NotEmpty
    private String type;

}

package pl.sda.askplus.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.jmx.export.annotation.ManagedAttribute;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor
public class Quiz {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String name;

    @JsonIgnore
    @ManyToOne
    private AppUser owner;

    private LocalDateTime createDate;

    private LocalDateTime validationTime;

    private String address;

    @JsonIgnore
    @OneToMany
    private List<Question> questionList = new ArrayList<>();

    @JsonIgnore
    @OneToMany
    private List<ResultQuiz> resultQuizsList;


}

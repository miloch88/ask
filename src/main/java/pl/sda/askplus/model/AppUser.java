package pl.sda.askplus.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String name;
    private String surname;

    @JsonIgnore
    private String password;

    private String email;

    @JsonIgnore
    @OneToMany
    private List<Quiz> quizList;

    @JsonIgnore
    @OneToMany
    private List<ResultQuiz> resultQuizsList;

}

package pl.sda.askplus.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String question;
    private String type;

    @JsonIgnore
    @ManyToOne
    private Quiz quiz;

    //single
    @JsonIgnore
    @OneToMany
    private List<Selections> selectionsList = new ArrayList<>();

//    single to było zmodyfikowane...
    @JsonIgnore
    @OneToMany
    private List<ResultQuestion> resultQuestionList;

////    many
    @JsonIgnore
    @ManyToMany(mappedBy = "resultQuestionList")
    private List<ResultQuestion> resultsQuestionList;

}
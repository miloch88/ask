package pl.sda.askplus.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.askplus.model.AppUser;
import pl.sda.askplus.model.Quiz;
import pl.sda.askplus.model.dto.AddQuizDto;
import pl.sda.askplus.model.dto.ModifyQuizDto;
import pl.sda.askplus.service.LoginService;
import pl.sda.askplus.service.QuizService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("api/quiz/")
public class QuizController {

    @Autowired
    private QuizService quizService;

    @Autowired
    private LoginService loginService;

    @GetMapping("/add")
    public String addQuiz(Model model) {
        model.addAttribute("new_quiz", new AddQuizDto());
        return "quiz/add";
    }

    @PostMapping("/add")
    public String addQuizPost(Model model, AddQuizDto dto) {

        if (dto.getName().isEmpty()) {
            model.addAttribute("new_quiz", dto);
            model.addAttribute("error_message", "Name of quiz is empty");
            return "quiz/add";
        }

        if (dto.getName().isEmpty()) {
            model.addAttribute("new_quiz", dto);
            model.addAttribute("error_message", "Validation date is empty");
            return "quiz/add";
        }

        Optional<Quiz> quizOptional = quizService.addQuiz(dto);
        Long quizId = quizOptional.get().getId();

        return "redirect:/api/quiz/manage?id="+quizId;
    }

    @GetMapping("/quiz_list")
    public String getQuizList(Model model){

        List<Quiz> quizList = quizService.getAll();
        AppUser appUser = loginService.getLoggedInUser().get();

        model.addAttribute("quiz_list", quizList);
        model.addAttribute("logged", appUser);

        return "quiz/list";
    }

    @GetMapping("/manage/")
    public String yourQuiz(Model model){

        List<Quiz> quizList = quizService.getAll();
        Long loggedId = loginService.getLoggedInUser().get().getId();

        model.addAttribute("your_quiz_list", quizList.stream()
        .filter(quiz -> quiz.getOwner().getId() == loggedId).collect(Collectors.toList()));

        return "quiz/manage";
    }

    @GetMapping("/manage")
    public String yourQuizPost(Model model,
                               @RequestParam(name = "id") Long id){

        List<Quiz> quizList = quizService.getAll();
        Long loggedId = loginService.getLoggedInUser().get().getId();

        model.addAttribute("your_quiz_list", quizList.stream()
                .filter(quiz -> quiz.getOwner().getId().equals(loggedId)).collect(Collectors.toList()));

        model.addAttribute("your_quiz", quizList.stream()
        .filter(quiz -> quiz.getId().equals(id)).collect(Collectors.toList()));

        return "quiz/manage";
    }

    @GetMapping("/modify/{id}")
    public String modify(Model model, @PathVariable(name = "id") Long id){
        Optional<Quiz> quizOptional = quizService.getById(id);

        if(!quizOptional.isPresent()){
            return "redirect:/api/quiz/manage";
        }

        ModifyQuizDto dto = new ModifyQuizDto();
        dto.setQuizId(id);

        model.addAttribute("quizId", id);
        model.addAttribute("modify_quiz", dto);

        return "quiz/modify";
    }

    @PostMapping("/modify/{id}")
    public String modifyPost(Model model, ModifyQuizDto dto,
                             @PathVariable(name= "id") Long id){

        Optional<Quiz> quizOptional = quizService.modifyQuiz(dto);

        List<Quiz> quizList = quizService.getAll();
        Long loggedId = loginService.getLoggedInUser().get().getId();

        model.addAttribute("your_quiz_list", quizList.stream()
                .filter(quiz -> quiz.getOwner().getId() == loggedId).collect(Collectors.toList()));

        model.addAttribute("your_quiz", quizList.stream()
                .filter(quiz -> quiz.getId() == id).collect(Collectors.toList()));

        model.addAttribute("quizId", id);

        return "quiz/manage";
    }

}

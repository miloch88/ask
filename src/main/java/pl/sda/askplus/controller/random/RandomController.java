package pl.sda.askplus.controller.random;

import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.askplus.model.Quiz;
import pl.sda.askplus.model.dto.random.RandomDto;
import pl.sda.askplus.service.random.RandomService;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("api/random")
public class RandomController {

    @Autowired
    private RandomService randomService;

    @GetMapping("/set")
    public String newRandomQuiz(Model model){

        List<String> randomList = randomService.getCategoryList();

        model.addAttribute("random_new", new RandomDto());
        model.addAttribute("random_list", randomList);

        return "random/set";
    }

    @PostMapping("/create")
    public String createRandomQuiz(Model model, RandomDto dto){

        List<String> randomList = randomService.getCategoryList();

        if (dto.getNumber() == null) {
            model.addAttribute("random_new", dto);
            model.addAttribute("random_list", randomList);
            model.addAttribute("error_message", "Number of question is empty");
            return "random/set";
        }

        if (dto.getValidationTime().isEmpty()) {
            model.addAttribute("random_new", dto);
            model.addAttribute("random_list", randomList);
            model.addAttribute("error_message", "Valid date is empty");
            return "random/set";
        }

        Optional<Quiz> randomQuiz = randomService.createNewQuiz(dto);

        return "redirect:/api/quiz/manage/";
    }


}

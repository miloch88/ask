package pl.sda.askplus.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.askplus.model.Quiz;
import pl.sda.askplus.model.Selections;
import pl.sda.askplus.model.dto.AddSelectionsDto;
import pl.sda.askplus.model.dto.ModifySelectionDto;
import pl.sda.askplus.service.LoginService;
import pl.sda.askplus.service.QuizService;
import pl.sda.askplus.service.SelectionsService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("api/answer")
public class SelectionsController {

    @Autowired
    private SelectionsService selectionsService;

    @Autowired
    private QuizService quizService;

    @Autowired
    private LoginService loginService;

    @GetMapping("/add")
    public String addAnswer(Model model, @RequestParam(name="id") Long id){

        AddSelectionsDto dto = new AddSelectionsDto();
        dto.setQuestionId(id);

        model.addAttribute("new_answer", dto);

        return "answer/add";
    }

    @PostMapping("/add")
    public String addAnswerPost(Model model, AddSelectionsDto dto){

        if(dto.getText().isEmpty()){
            model.addAttribute("new_answer", dto);
            model.addAttribute("error_message", "Answer is empty");
            return "answer/add";
        }
        Optional<Selections> optionalSelections = selectionsService.addQuestion(dto);
        Long quizId = optionalSelections.get().getQuestion().getQuiz().getId();

        return "redirect:/api/quiz/manage?id="+quizId;
    }

    @GetMapping("/modify/{id}")
    public String modify(Model model, @PathVariable(name="id") Long id){
        Optional<Selections> selectionsOptional = selectionsService.findById(id);

        if(!selectionsOptional.isPresent()){
            return "redirect:/api/quiz/manage";
        }

        ModifySelectionDto dto = new ModifySelectionDto();
        dto.setSelectionId(id);

        model.addAttribute("selectionId", id);
        model.addAttribute("modify_selection", dto);

        return "answer/modify";
    }

    @PostMapping("/modify/{id}")
    public String modifyPost(Model model, ModifySelectionDto dto,
                             @PathVariable(name="id") Long id){

        Optional<Selections> selectionsOptional = selectionsService.modifySelection(dto);
        Long quizId = selectionsOptional.get().getQuestion().getQuiz().getId();
        model.addAttribute("quizId", quizId);

        List<Quiz> quizList = quizService.getAll();
        Long loggedId = loginService.getLoggedInUser().get().getId();

        model.addAttribute("your_quiz_list", quizList.stream()
                .filter(quiz -> quiz.getOwner().getId() == loggedId).collect(Collectors.toList()));

        model.addAttribute("your_quiz", quizList.stream()
                .filter(quiz -> quiz.getId() == id).collect(Collectors.toList()));

        return "redirect:/api/quiz/manage?id="+ quizId;

    }

}

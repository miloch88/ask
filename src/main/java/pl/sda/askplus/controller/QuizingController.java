package pl.sda.askplus.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.askplus.model.Quiz;
import pl.sda.askplus.model.ResultQuestion;
import pl.sda.askplus.model.ResultQuiz;
import pl.sda.askplus.model.dto.AddResultQuizDto;
import pl.sda.askplus.model.dto.quiz.AnswerDto;
import pl.sda.askplus.model.dto.quiz.QuizDto;
import pl.sda.askplus.model.dto.quiz.QuizQuestionDto;
import pl.sda.askplus.service.QuizService;
import pl.sda.askplus.service.ResultQuestionService;
import pl.sda.askplus.service.ResultQuizService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping("/quizme/")
public class QuizingController {

    @Autowired
    private QuizService quizService;

    @Autowired
    private ResultQuizService resultQuizService;

    @Autowired
    private ResultQuestionService resultQuestionService;

    @GetMapping("/getquiz/{address}")
    public String getQuiz(Model model, @PathVariable(name = "address") String address) {

        Optional<Quiz> quiz = quizService.getByUniqeNumber(address);
        Long quizId = quiz.get().getId();

        AddResultQuizDto dto = new AddResultQuizDto();
        dto.setQuizId(quizId);

        String uniqeNumber = UUID.randomUUID().toString();
        dto.setUniqeNumber(uniqeNumber);

        Optional<ResultQuiz> resultQuizOptional = resultQuizService.addResultQuiz(dto);

        if (quiz.isPresent()) {
            Quiz q = quiz.get();
            QuizDto quizDto = new QuizDto();
            quizDto.setQuizId(q.getId());

            getQuestionListStream(q, quizDto);

            model.addAttribute("uniqueNumber", uniqeNumber);
            model.addAttribute("quizId", quizDto.getQuizId());
            model.addAttribute("currentQuestion", quizDto.getQuizQuestions().get(0));
            model.addAttribute("currentQuestionNumber", 0);


            return "quiz/quizing";
        }
        return "redirect:/";
    }


    @PostMapping("/answer/{uniqeNumber}/{id}/{questionNumber}")
    public String answerQuizQuestion(Model model, QuizQuestionDto dto,
                                     @PathVariable(name = "uniqeNumber") String uniqeNumber,
                                     @PathVariable(name = "id") Long quizid,
                                     @PathVariable(name = "questionNumber") Long questionNumber) {

        System.out.println(dto.getAnswerDtoList());

//         Pusta odpowiedź
        if (dto.getAnswerId() == null) {

            Quiz q = quizService.getById(quizid).get();
            QuizDto quizDto = new QuizDto();
            quizDto.setQuizId(q.getId());

            getQuestionListStream(q, quizDto);

            model.addAttribute("uniqueNumber", uniqeNumber);
            model.addAttribute("quizId", quizDto.getQuizId());
            model.addAttribute("currentQuestion", quizDto.getQuizQuestions().get(Math.toIntExact(questionNumber)));
            model.addAttribute("currentQuestionNumber", questionNumber);

            model.addAttribute("error_message", "You did not answer");

            return "quiz/quizing";
        }

    
        Optional<Quiz> quizOptional = quizService.getById(quizid);

        ResultQuiz resultQuiz = resultQuizService.getByUniqueNumber(uniqeNumber).get();
        Optional<ResultQuestion> resultQuestion =
                resultQuestionService.addResultQuestion(dto, resultQuiz, quizid, questionNumber);
        resultQuiz = resultQuizService.setCompletedTime(resultQuiz);

         if (quizOptional.get().getQuestionList().size() -1  > questionNumber) {

            if (quizOptional.isPresent()) {
                Quiz q = quizOptional.get();
                QuizDto quizDto = new QuizDto();
                quizDto.setQuizId(q.getId());

                getQuestionListStream(q, quizDto);

                model.addAttribute("uniqueNumber", uniqeNumber);
                model.addAttribute("quizId", quizid);
                model.addAttribute("currentQuestion", quizDto.getQuizQuestions().get((int) (questionNumber + 1)));
                model.addAttribute("currentQuestionNumber", questionNumber + 1);

                return "quiz/quizing";
            }
        }

        resultQuiz = resultQuizService.setCompletedTime(resultQuiz);

        return "redirect:/api/user/results/" + resultQuiz.getId();
    }

    private void getQuestionListStream(Quiz q, QuizDto quizDto) {
        q.getQuestionList().stream()
                .forEach(question -> {
                    List<AnswerDto> answers = new ArrayList<>();

                    question.getSelectionsList().stream()
                            .forEach(selections -> {
                                answers.add(new AnswerDto(selections.getId(),
                                        selections.getText(), false));
                            });

                    quizDto.getQuizQuestions().add(
                            new QuizQuestionDto(question.getQuestion(),
                                    answers, -1L, question.getType()));
                });
    }

}

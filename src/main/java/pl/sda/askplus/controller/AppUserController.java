package pl.sda.askplus.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.askplus.model.AppUser;
import pl.sda.askplus.model.Quiz;
import pl.sda.askplus.model.ResultQuiz;
import pl.sda.askplus.model.dto.ModifyAppUserDto;
import pl.sda.askplus.model.dto.ModifyPasswordDto;
import pl.sda.askplus.service.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("api/user")
public class AppUserController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private ResultQuizService resultQuizService;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @GetMapping("/profile")
    public String profile(Model model) {

        Optional<AppUser> appUserOptional = loginService.getLoggedInUser();

        if (appUserOptional.isPresent()) {
            AppUser appUser = appUserOptional.get();
            List<Quiz> quizList = appUser.getQuizList();


            model.addAttribute("logged", appUser);
            model.addAttribute("quizList", quizList);

            return "appUser/profile";
        }
        return "/login";
    }

    @GetMapping("/modify")
    public String modify(Model model, @RequestParam(name = "id") Long id) {
        Optional<AppUser> appUserOptional = appUserService.findById(id);

        if (!appUserOptional.isPresent()) {
            return "redirect:/api/user/profile";
        }
        ModifyAppUserDto dto = new ModifyAppUserDto();
        dto.setAppUserId(id);

        model.addAttribute("modify_user", dto);

        return "appUser/modify_user";
    }

    @PostMapping("/modify")
    public String modifyPost(ModifyAppUserDto dto) {
        Optional<AppUser> optionalAppUser = appUserService.modifyUser(dto);
        return "redirect:/api/user/profile";
    }

    @GetMapping("/change_password")
    public String changePassword(Model model, @RequestParam(name = "id") Long id) {
        Optional<AppUser> appUserOptional = appUserService.findById(id);

        if (!appUserOptional.isPresent()) {
            return "redirect:/api/user/profile";
        }

        ModifyPasswordDto dto = new ModifyPasswordDto();
        dto.setAppUserId(id);

        model.addAttribute("change_password", dto);
        return "appUser/modify_password";
    }

    @PostMapping("/change_password")
    public String changePassword(Model model, ModifyPasswordDto dto) {

        AppUser appUser = appUserService.findById(dto.getAppUserId()).get();

        if (!encoder.matches(dto.getPassword(), appUser.getPassword())) {
            model.addAttribute("change_password", dto);
            model.addAttribute("error_message", "Old password is not correct");
            return "appUser/modify_password";
        }

        if (dto.getNewPassword().isEmpty() || dto.getNewPassword().length() < 5) {
            model.addAttribute("change_password", dto);
            model.addAttribute("error_message", "New password is too weak");
            return "appUser/modify_password";
        }

        if (!dto.getNewPassword().equals(dto.getNewPasswordConfirm())) {
            model.addAttribute("change_password", dto);
            model.addAttribute("error_message", "New password is not confirmed");
            return "appUser/modify_password";
        }

        Optional<AppUser> optionalAppUser = appUserService.modifyPassword(dto);

        return "redirect:/api/user/profile";
    }

    @GetMapping("/your_results")
    public String yourResult(Model model) {

        List<ResultQuiz> resultQuizList = resultQuizService.getAll();
        Long loggedId = loginService.getLoggedInUser().get().getId();

        model.addAttribute("your_results", resultQuizList.stream()
                .filter(resultQuiz -> resultQuiz.getAppUser().getId() == loggedId).collect(Collectors.toList()));

        return "appUser/your_results";
    }

    @PostMapping("/your_results")
    public String yourResultPost(Model model,
                                 @RequestParam(name = "id", required = true) Long id) {

        Long loggedId = loginService.getLoggedInUser().get().getId();
        List<ResultQuiz> resultQuizList = resultQuizService.getAll();

        model.addAttribute("your_results", resultQuizList.stream()
                .filter(resultQuiz -> resultQuiz.getAppUser().getId() == loggedId).collect(Collectors.toList()));

        List<ResultQuiz> resultQuizList2 = resultQuizService.getAll();
        model.addAttribute("your_result", resultQuizList2.stream()
                .filter(resultQuiz -> resultQuiz.getId().equals(id)).collect(Collectors.toList()).get(0));

        ResultQuiz resultQuizCalculate = resultQuizList2.stream()
                .filter(resultQuiz -> resultQuiz.getId().equals(id)).collect(Collectors.toList()).get(0);

        Integer score = resultQuizService.calculateScore(resultQuizList2.stream()
                .filter(resultQuiz -> resultQuiz.getId().equals(id)).collect(Collectors.toList()).get(0));

        model.addAttribute("score",score);

        return "appUser/your_results";
    }

    @GetMapping("/results/{id}")
    public String yourResultPostPath(Model model,
                                 @PathVariable(name = "id", required = true) Long id) {

        List<ResultQuiz> resultQuizList = resultQuizService.getAll();
        model.addAttribute("result", resultQuizList.stream()
                .filter(resultQuiz -> resultQuiz.getId().equals(id)).collect(Collectors.toList()).get(0));

        Integer score = resultQuizService.calculateScore(resultQuizList.stream()
                .filter(resultQuiz -> resultQuiz.getId().equals(id)).collect(Collectors.toList()).get(0));

        model.addAttribute("score",score);

        return "appUser/result";
    }

}

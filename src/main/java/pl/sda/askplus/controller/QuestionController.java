package pl.sda.askplus.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.askplus.model.Question;
import pl.sda.askplus.model.Quiz;
import pl.sda.askplus.model.dto.AddQuestionDto;
import pl.sda.askplus.model.dto.ModifyQuestionDto;
import pl.sda.askplus.model.dto.ModifyQuizDto;
import pl.sda.askplus.service.LoginService;
import pl.sda.askplus.service.QuestionService;
import pl.sda.askplus.service.QuizService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("api/question")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @Autowired
    private QuizService quizService;

    @Autowired
    private LoginService loginService;

    @GetMapping("/add")
    public String addQuestion(Model model, @RequestParam(name = "id") Long id) {

        AddQuestionDto dto = new AddQuestionDto();
        dto.setQuizId(id);

        model.addAttribute("type", new ArrayList<>(Arrays.asList("single", "multiple")));
        model.addAttribute("new_question", dto);

        return "question/add";
    }

    @PostMapping("/add")
    public String addQuestionPost(Model model, AddQuestionDto dto) {

        if (dto.getQuestion().isEmpty()) {
            model.addAttribute("new_question", dto);
            model.addAttribute("error_message", "Question is empty");
            return "question/add";

        }
        Optional<Question> questionOptional = questionService.addQuestion(dto);
        Long quizId = questionOptional.get().getQuiz().getId();

        return "redirect:/api/quiz/manage?id="+quizId;
    }

    @GetMapping("/modify/{id}")
    public String mofidy(Model model, @PathVariable(name="id") Long id){
        Optional<Question> questionOptional = questionService.getById(id);

        if(!questionOptional.isPresent()){
            return "redirect:/api/quiz/manage";
        }

        ModifyQuestionDto dto = new ModifyQuestionDto();
        dto.setQuestionId(id);

        model.addAttribute("questionId", id);
        model.addAttribute("modify_question", dto);

        return "question/modify";
    }

    @PostMapping("/modify/{id}")
    public String modifyPost(Model model, ModifyQuestionDto dto,
                             @PathVariable(name="id") Long id){

        Optional<Question> questionOptional = questionService.modifyQuestion(dto);
        Long quizId = questionOptional.get().getQuiz().getId();
        model.addAttribute("quizId", quizId);

        List<Quiz> quizList = quizService.getAll();
        Long loggedId = loginService.getLoggedInUser().get().getId();

        model.addAttribute("your_quiz_list", quizList.stream()
                .filter(quiz -> quiz.getOwner().getId() == loggedId).collect(Collectors.toList()));

        model.addAttribute("your_quiz", quizList.stream()
                .filter(quiz -> quiz.getId() == id).collect(Collectors.toList()));

            return "redirect:/api/quiz/manage?id="+quizId;
    }
}

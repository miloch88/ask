package pl.sda.askplus.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.askplus.model.AppUser;
import pl.sda.askplus.model.dto.RegisterDto;
import pl.sda.askplus.service.AppUserService;
import pl.sda.askplus.service.RegisterService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/employees")
public class EmployeeApiController {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private RegisterService registerService;

    @GetMapping("/")
    public ResponseEntity<List<AppUser>> getEmployees(){

        return ResponseEntity.ok(appUserService.getAll());

    }

    @PostMapping("/add")
    public  ResponseEntity<AppUser> addAppUser(@RequestBody RegisterDto dto){
        registerService.registerAppUser(dto);
        return ResponseEntity.ok().build();
    }
}

package pl.sda.askplus.controller;

import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.askplus.model.Quiz;
import pl.sda.askplus.model.ResultQuiz;
import pl.sda.askplus.model.dto.statistic.StatisticQuizDto;
import pl.sda.askplus.service.QuizService;
import pl.sda.askplus.service.ResultQuizService;

import javax.websocket.server.PathParam;
import java.util.*;

@Controller
@RequestMapping("api/statistic")
public class StatisticController {

    @Autowired
    private QuizService quizService;

    @Autowired
    private ResultQuizService resultQuizService;

    @GetMapping("/quiz/{id}")
    public String statistic(Model model,
                            @PathVariable(name = "id") Long id,
                            @RequestParam(name = "sort", required = false) Optional<String> sort) {

        Quiz quiz = quizService.getById(id).get();
        List<ResultQuiz> resultQuizsList = quiz.getResultQuizsList();

        List<StatisticQuizDto> statisticQuizDtoList = resultQuizService.createStatisticListDto(resultQuizsList);

        if (sort.isPresent()) {
            String sortBy = sort.get();
            if (sortBy.equals("User")) {
                resultQuizService.comparableByUser(statisticQuizDtoList);
            }
            if (sortBy.equals("Date")) {
                resultQuizService.comparableByDate(statisticQuizDtoList);
            }
            if (sortBy.equals("Score")) {
                resultQuizService.comparableByScore(statisticQuizDtoList);
            }
        }

        Integer numberQuestion = quiz.getQuestionList().size();
        Integer attempsSolve = resultQuizsList.size();
        Integer completed = resultQuizService.getCompleted(quiz);
        Double correctAnswer = resultQuizService.calculateGeneralScore(quiz);

        model.addAttribute("quiz", quiz);
        model.addAttribute("number", numberQuestion);
        model.addAttribute("attemps", attempsSolve);
        model.addAttribute("completed", completed);
        model.addAttribute("correct", String.format("%.2f", correctAnswer));
        model.addAttribute("resultList", statisticQuizDtoList);
        model.addAttribute("sortBy", new ArrayList<>(Arrays.asList("User", "Date", "Score")));

        return "quiz/stat";
    }
//
//    @GetMapping("/quiz/{id}/sort")
//    public String statistic(Model model, List<StatisticQuizDto> statisticQuizDtoList,
//                            @PathVariable(name = "id") Long id,
//                            @RequestParam(name = "sort") String sort){
//
//        Quiz quiz = quizService.getById(id).get();
//        List<ResultQuiz> resultQuiz = quiz.getResultQuizsList();
//
//        Integer numberQuestion = quiz.getQuestionList().size();
//        Integer attempsSolve = resultQuiz.size();
//        Integer completed = resultQuizService.getCompleted(quiz);
//        Double correctAnswer = resultQuizService.calculateGeneralScore(quiz);
//
//
//
//        model.addAttribute("quiz", quiz);
//        model.addAttribute("number", numberQuestion);
//        model.addAttribute("attemps", attempsSolve);
//        model.addAttribute("completed", completed);
//        model.addAttribute("correct", String.format("%.2f", correctAnswer));
//        model.addAttribute("resultList", statisticQuizDtoList);
//        model.addAttribute("sortBy", new ArrayList<>(Arrays.asList("User", "Date", "Score")));
//
//        return "quiz/stat";
//    }
}

package pl.sda.askplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AskplusApplication {

    public static void main(String[] args) {
        SpringApplication.run(AskplusApplication.class, args);
    }

}
